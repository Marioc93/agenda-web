contactos = [];

function cargarTabla(contactos){
  var xmlhttp = new XMLHttpRequest();
  var url = "cargarContactos.php";
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var json = JSON.parse(this.response);

      contactos = json;
      contactos.forEach((c) => {
        var tablaContactos = document.getElementById("contactos");
        var filaCreada = document.createElement('tr');
        var celda0 = document.createElement('td');
        var celda1 = document.createElement('td');
        var celda2 = document.createElement('td');
        celda0.textContent = c.nombre;
        celda1.textContent = c.apellido;
        celda2.textContent = c.telefono;
        filaCreada.appendChild(celda0);
        filaCreada.appendChild(celda1);
        filaCreada.appendChild(celda2);

        //Creo celda para editar el contacto
        var celda3 = document.createElement('td');
        var enlaceEditar = document.createElement('a');
        var enlace = document.createTextNode("Editar");
        enlaceEditar.appendChild(enlace);
        enlaceEditar.href = "actualizar.html";
        celda3.appendChild(enlaceEditar);
        filaCreada.appendChild(celda3);

        //Creo celda para eliminar el contacto
        var celda4 = document.createElement('td');
        var enlaceEliminar = document.createElement('a');
        var enlaceEl = document.createTextNode("Eliminar");
        enlaceEliminar.appendChild(enlaceEl);
        enlaceEliminar.href = "https://www.mercadolibre.com.ar";
        celda4.appendChild(enlaceEliminar);
        filaCreada.appendChild(celda4);

        tablaContactos.appendChild(filaCreada);
      });
    }
  };

  xmlhttp.open("GET", url);
  xmlhttp.send();

}
